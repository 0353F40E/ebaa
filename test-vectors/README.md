# Test Vectors

Provided scripts will generate headerless `.csv` datasets and test vectors, as well as `.json` test vectors to be used to test implementations against.
The `generate.sh` script will:

- Produce full testing datasets as `.csv` files; These files are not included in the repo, but their hashes are.
- From generated datasets, extract 100-block test vectors as `.csv` and `.json` files in [`./csv`](./csv) and [`./json`](./json); These files are included in the repo.

### `.csv` Format

Columns are defined here as:

1. Block height `n`;
2. Size of block with height `n`;
3. Limit to be enforced for the next block at height `n+1`;
4. Algorithm's internal state `elasticBufferSize` at height `n+1`;
5. Algorithm's internal state `controlBlockSize` at height `n+1`.

Example:

```
99949,9780293,32000000,16000000,16000000
99950,30403449,32000000,16000000,16000000
...
100000,27586098,32001416,16001249,16000167
100001,22123312,32002238,16001958,16000280
```

### `.json` Format

The `.json` file include additional data and metadata:

1. ABLAConfig, a record of algorithm configuration used for the test vector;
2. testDescription, test vector description;
3. ABLAStateInitial, a record of algorithm's starting state;
4. testVector, an array of block sizes and resulting algorithm's internal states:
	1. Size of block with height `n`;
	2. ABLAState for block `n+1`;
	3. Blocksize limit for block `n+1`.

Example:

```
{
  "ABLAConfig": {
    "epsilon0": "16000000",
    "beta0": "16000000",
    "n0": 100000,
    "zeta": 37938,
    "gammaReciprocal": 192,
    "delta": 37938,
    "thetaReciprocal": 10
  },
  "testDescription": "Test activation at configured height; using a random sampling in the 0 to 32 MB range; using proposed configuration with the exception of n0 which will be determined from MTP activation.",
  "ABLAStateInitial": {
    "n": 99950,
    "epsilon": "16000000",
    "beta": "16000000"
  },
  "blocksizeLimitInitial": "32000000",
  "testVector": [
    {
      "blocksize": "30403449",
      "ABLAStateForNextBlock": {
        "n": 99951,
        "epsilon": "16000000",
        "beta": "16000000"
      },
      "blocksizeLimitForNextBlock": "32000000"
    },
	...
    {
      "blocksize": "5738094",
      "ABLAStateForNextBlock": {
        "n": 100000,
        "epsilon": "16000000",
        "beta": "16000000"
      },
      "blocksizeLimitForNextBlock": "32000000"
    },
    {
      "blocksize": "27586098",
      "ABLAStateForNextBlock": {
        "n": 100001,
        "epsilon": "16000167",
        "beta": "16001249"
      },
      "blocksizeLimitForNextBlock": "32001416"
    },
	...
  ]
}

```

## Building

1. Build the 2 implementations (`../implementation-c` and `../implementation-cpp`);
2. Run `./make-prng.sh` which will build and test a simple uint64 PRNG utility;
3. Run `./generate.sh` which will generate `.csv` files with test vectors;
4. Verify generated files with `./verify.sh`.

## Tests

Configurations:

- Configuration 1: `-excessiveblocksize 32000000 -ablaconfig 16000000,100000,37938,192,37938,10`; This is the proposed configuration with the exception of n0 which will be determined from MTP activation.
- Configuration 2: `-excessiveblocksize 32000000 -ablaconfig 16000000,100000,9484,256,151744,32`; This is a testing configuration, at the extreme of sanity range of configuration parameters.

Generators:

- Test 1: using sizes from `../backtesting/datasets/hypothetical-hybrid-x64.csv`;
- Test 2: random sizes using ranges: 0 to 32 MB, 0 to 128 MB, 0 to 512 MB, 0 to 2048 MB, 0 to 8196 MB;
- Test 3: random sizes in full uint64 range;
- Test 4: sequence of `UINT64_MAX` sizes;
- Test 5: sequence of 8196 MB sizes;
- Test 6: sequence of `UINT64_MAX`, then sequence of `0` sizes.

Note: the generating program clips input to the current limit's state, so resulting output and test vectors will have sizes exactly at the limit for those cases.